import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:

    df = pd.read_csv("train.csv")

    return df


def get_filled():

    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss."]
    final = []

    for title in titles:

        median_age = df[df["Name"].str.contains(titles)]["Age"].median()
        miss_val = df[df["Name"].str.contains(title)]["Age"].isnull().sum()
        final.append((title, miss_val, int(round(median_age))))

    return final
